package com.example.shadowtask050421.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import com.example.shadowtask050421.R;
import com.example.shadowtask050421.Utils.Data;
import com.example.shadowtask050421.Utils.UIUtils;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class ScanActivity extends AppCompatActivity {

    private SurfaceView mSurfaceViewQRCode;
    private TextView mTextViewText;
    private BarcodeDetector mBarCodeDetector;
    private CameraSource mCameraSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        // initialize views
        mSurfaceViewQRCode = findViewById(R.id.sv_qr);
    }

    /**
     * Release the cameraSource when Activity is paused
     */
    @Override
    protected void onPause() {

        super.onPause();
        mCameraSource.release();
    }

    /**
     * Initialize scanning and detection when Activity is live
     */
    @Override
    protected void onResume() {

        super.onResume();
        initialiseDetector();
    }

    /**
     * Initialize scanning and detection
     */
    private void initialiseDetector() {

        // Build BarCodeDetector object
        mBarCodeDetector = new BarcodeDetector.Builder(ScanActivity.this).build();
        // Build cameraSource object
        mCameraSource = new CameraSource.Builder(ScanActivity.this, mBarCodeDetector)
                .setAutoFocusEnabled(true)
                .build();
        // Get Holder i.e. gives access to surface under SurfaceView
        mSurfaceViewQRCode.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(@NonNull SurfaceHolder holder) {
                try {
                    // Check if camera access permission is granted
                    if (ActivityCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                        // start cameraSource
                        mCameraSource.start(mSurfaceViewQRCode.getHolder());
                    } else {
                        // request permission
                        ActivityCompat.requestPermissions(
                                ScanActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                Data.SCAN_REQUEST_CAMERA_PERMISSION);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

            }

            // stop cameraSource
            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
                mCameraSource.stop();
            }
        });
        // Set a processor that processes the detections received
        mBarCodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                // SparseArray maps integers to Objects
                // more memory efficient than HashMap
                // uses binary search to find keys
                final SparseArray<Barcode> barcodeSparseArray = detections.getDetectedItems();
                if (barcodeSparseArray.size() > 0) {
                    // create a new intent to send barcode
                    Intent intent = new Intent();
                    intent.putExtra(Data.SCAN_EXTRA_BARCODE, barcodeSparseArray.valueAt(0));
                    // send intent as result
                    setResult(CommonStatusCodes.SUCCESS, intent);
                    // finish ScanActivity
                    finish();
//                    if (barcodeSparseArray.valueAt(0) != null) {
//                        Log.v("YOP", barcodeSparseArray.valueAt(0).rawValue);
//                        mTextViewText.removeCallbacks(null);
//                        mTextViewText.setText(barcodeSparseArray.valueAt(0).toString());
//                    }
//                    mTextViewText.post(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });
                }
            }
        });
    }
}