package com.example.shadowtask050421.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shadowtask050421.R;
import com.example.shadowtask050421.Utils.Data;
import com.example.shadowtask050421.Utils.EncodeDecode;
import com.example.shadowtask050421.Utils.UIUtils;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.WriterException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class MainActivity extends AppCompatActivity {

    private EditText mEditTextText;
    private ImageView mImageViewQRCode;
    private Button mButtonGenerate, mButtonScan;
    private Bitmap mBitmap;
    private TextView mTextViewResult;
    private static final String PASSWORD = "sakivg12345678yo";
    private static final String LOG_TAG = "sakivg";
    private static final String AES = "AES";

    // Not required
    private static String salt = "1234578";
    private static IvParameterSpec ivParameterSpec = EncodeDecode.generateIv();
    private static SecretKey secretKey;
    private static final byte[] initializationVector = new byte[16];
    private static SecureRandom secureRandom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // disable screenshot feature
        // adding FLAG_SECURE to your activity
        // Window flag: treat the content of the window as secure,
        // preventing it from appearing in screenshots or from being viewed on non-secure displays.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);
        // declare & initialize member variables
        this.initMemberVars();
//        try{
//            secretKey = createSecretKey(password);
//        }catch(Exception e){
//            Log.v(LOG_TAG, e.getMessage());
//            e.printStackTrace();
//            return;
//        }
        mButtonGenerate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String text = mEditTextText.getText().toString().trim();
                // hide keyboard after 'Generate' button is clicked
                hideSoftKeyboard();
                if (TextUtils.isEmpty(text)) {
                    // if edit text is empty
                    UIUtils.showShortToast(MainActivity.this, "Fill the field");
                } else {
                    try {
                        String encryptedText = encrypt(text, PASSWORD);
                        Log.v(LOG_TAG, encryptedText);
                        // Generate QR code
                        generateQRCode(encryptedText, 300);
//                        secretKey = EncodeDecode.generateKey(password);
//                        String encryptedText = new String(EncodeDecode.encryptMsg(text, secretKey));
//                        String encryptedText = new String(EncodeDecode.encryptMsg(text, secretKey, initializationVector), "UTF-8");
//                        String encryptedText = EncodeDecode.encrypt(text, secretKey, ivParameterSpec);

                    } catch (Exception e) {
                        // Error
                        UIUtils.showShortToast(MainActivity.this, "Unable to generate QR code");
                        e.printStackTrace();
                    }
                }
            }
        });
        mButtonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // take to ScanActivity
                startActivityForResult(new Intent(MainActivity.this, ScanActivity.class), 0);
            }
        });
    }

    /**
     * Initialize all member variables
     */
    private void initMemberVars() {

        mButtonGenerate = findViewById(R.id.btn_generate);
        mButtonScan = findViewById(R.id.btn_goToScan);
        mEditTextText = findViewById(R.id.et_text);
        mImageViewQRCode = findViewById(R.id.iv_qrcodegen);
        mTextViewResult = findViewById(R.id.tv_text);
    }

//    private SecretKey createSecretKey(String password) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
//        secureRandom = new SecureRandom();
//        secureRandom.nextBytes(initializationVector);
//
////        return EncodeDecode.generateKey(password, salt);
//        return EncodeDecode.generateKey(password);
//    }

    /**
     * When ScanActivity is finished and MainActivity is in focus again
     * @param requestCode 0
     * @param resultCode 0
     * @param data intent with Barcode
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Data.MAIN_TO_SCAN_REQUEST_CODE && resultCode == CommonStatusCodes.SUCCESS) {
            if (data != null) {
                // intent is there
                Barcode barcode = data.getParcelableExtra("barcode");
                // get value from barcode
                String displayValue = barcode.displayValue;
//                byte[] displayValueByteArray = displayValue.getBytes();
                try {
                    String decryptedText = decrypt(displayValue, PASSWORD);
                    mTextViewResult.setText(decryptedText);
//                    secretKey = EncodeDecode.generateKey();
//                    mTextViewResult.setText(EncodeDecode.decrypt("AES/CBC/PKCS5Padding", new String(displayValueByteArray), secretKey, ivParameterSpec));
//                    mTextViewResult.setText(EncodeDecode.decryptMsg(displayValueByteArray, secretKey));
//                    mTextViewResult.setText(EncodeDecode.decryptMsg(displayValueByteArray, secretKey, initializationVector));
                } catch (Exception e) {
                    e.printStackTrace();
                    UIUtils.showShortToast(MainActivity.this, "Unable to decrypt");
                }
            } else {
                mTextViewResult.setText("Barcode not detected");
            }
        }
    }

    /**
     * Generate QR code & set it to the mImageViewQRCode
     * @param inputValue text
     * @param smallerDimension size of the QR code
     */
    private void generateQRCode(String inputValue, int smallerDimension) {

        // Initializing the QR Encoder with your value to be encoded, type you required and Dimension
        QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, smallerDimension);
//        qrgEncoder.setColorBlack(Color.RED);
//        qrgEncoder.setColorWhite(Color.BLUE);
        // Getting QR-Code as Bitmap
        mBitmap = qrgEncoder.getBitmap();
        // Setting Bitmap to ImageView
        mImageViewQRCode.setImageBitmap(mBitmap);
    }

    /**
     * Hides the Soft Keyboard
     */
    public void hideSoftKeyboard(){

        // Gets the current view with focus
        final View view = getCurrentFocus();
        if(view != null){
            // InputMethodManager communicates with a global system service
            InputMethodManager imm =(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            // interface(token i.e. identifier for this window)
            // for communication with the bound service
            IBinder iBinder = view.getWindowToken();
            imm.hideSoftInputFromWindow(iBinder, 0);
        }
    }

    /**
     * Decrypts string
     * @param outputData encrypted text
     * @param password password
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private String decrypt(String outputData, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        SecretKeySpec key = generateKey(password);
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decVal = Base64.decode(outputData, Base64.DEFAULT);
        String decrypted = new String(cipher.doFinal(decVal));
        return decrypted;
    }


    private String encrypt(String data, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException {

        SecretKeySpec key = generateKey(password);
        //The transformation AES/ECB/PKCS5Padding tells the getInstance method to instantiate the Cipher object
        // as an AES cipher with ECB mode of operation and PKCS5 padding scheme.
        // Cipher class is not thread safe
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = cipher.doFinal(data.getBytes());
        String encrypted = Base64.encodeToString(encVal, Base64.DEFAULT);
        return encrypted;
    }

    private SecretKeySpec generateKey(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        // hash values = message digest
        // java.security
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = password.getBytes("UTF-8");
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        // if algo is not specified key is simply "PBKDF2WithHmacSHA1" key not AES key
        // to make it AES key
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

}