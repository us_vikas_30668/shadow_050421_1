//package com.example.shadowtask050421.Utils;
//
//import android.os.Build;
//
//import androidx.annotation.RequiresApi;
//
//import java.io.UnsupportedEncodingException;
//import java.security.InvalidAlgorithmParameterException;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.InvalidParameterSpecException;
//import java.security.spec.KeySpec;
//import java.util.Arrays;
//import java.util.Base64;
//
//import javax.crypto.BadPaddingException;
//import javax.crypto.Cipher;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.KeyGenerator;
//import javax.crypto.NoSuchPaddingException;
//import javax.crypto.SecretKey;
//import javax.crypto.SecretKeyFactory;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.PBEKeySpec;
//import javax.crypto.spec.SecretKeySpec;
//
//public class EncodeDecode {
//
//    public static SecretKey generateKey(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
//
////        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
////        keyGenerator.init(256);
////        return keyGenerator.generateKey();
//        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
//        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 256);
//        SecretKey secretKey = new SecretKeySpec(factory.generateSecret(keySpec).getEncoded(), "AES");
//        return secretKey;
//    }
//
//    public static SecretKey generateKey(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
//
//        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
//        keyGenerator.init(256);
//        return keyGenerator.generateKey();
////        return new SecretKeySpec(password.getBytes(), "AES");
//    }
//
//    public static IvParameterSpec generateIv() {
//
//        byte[] iv = new byte[16];
//        new SecureRandom().nextBytes(iv);
//        return new IvParameterSpec(iv);
//    }
//
//    public static byte[] encryptMsg(String message, SecretKey secretKey, byte[] IV)
//            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
//            IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
//
//        Cipher cipher = Cipher.getInstance("AES");
//        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(IV);
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
//        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
//        return cipherText;
//    }
//
//    public static byte[] encryptMsg(String message, SecretKey secret)
//            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
//    {
//        /* Encrypt the message. */
//        Cipher cipher = null;
//        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//        cipher.init(Cipher.ENCRYPT_MODE, secret);
//        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
//        return cipherText;
//    }
//
//    public static String decryptMsg(byte[] cipherText, SecretKey secret)
//            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException
//    {
//        /* Decrypt the message, given derived encContentValues and initialization vector. */
//        Cipher cipher = null;
//        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//        cipher.init(Cipher.DECRYPT_MODE, secret);
//        String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
//        return decryptString;
//    }
//
//
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    public static String encrypt(String input, SecretKey key,
//                                 IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
//            InvalidAlgorithmParameterException, InvalidKeyException,
//            BadPaddingException, IllegalBlockSizeException {
//
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
//        byte[] cipherText = cipher.doFinal(input.getBytes());
//        return Base64.getEncoder()
//                .encodeToString(cipherText);
//    }
//
//    public static String decryptMsg(byte[] cipherText, SecretKey secretKey, byte[] IV)
//            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException
//    {
//
//        Cipher cipher = Cipher.getInstance("AES");
//        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
//        IvParameterSpec ivParameterSpec = new IvParameterSpec(Arrays.copyOf(IV, IV.length));
//        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
//        String decryptString = new String(cipher.doFinal(cipherText));
//        return decryptString;
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    public static String decrypt(String algorithm, String cipherText, SecretKey key,
//                                 IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
//            InvalidAlgorithmParameterException, InvalidKeyException,
//            BadPaddingException, IllegalBlockSizeException {
//
//        Cipher cipher = Cipher.getInstance(algorithm);
//        cipher.init(Cipher.DECRYPT_MODE, key, iv);
//        byte[] plainText = cipher.doFinal(Base64.getDecoder()
//                .decode(cipherText));
//        return new String(plainText);
//    }
//}
