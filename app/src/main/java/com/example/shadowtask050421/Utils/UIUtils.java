package com.example.shadowtask050421.Utils;

import android.content.Context;
import android.widget.Toast;

public class UIUtils {

    public static void showShortToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
