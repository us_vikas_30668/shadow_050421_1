package com.example.shadowtask050421.Utils;

public class Data {

    // MainActivity
    public static final int MAIN_TO_SCAN_REQUEST_CODE = 0;
    // ScanActivity
    public static final String SCAN_EXTRA_BARCODE = "barcode";
    public static final int SCAN_REQUEST_CAMERA_PERMISSION = 201;
}
